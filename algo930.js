var KiteConnect = require("kiteconnect").KiteConnect;
const puppeteer = require('puppeteer');
const bnfFuture = "NFO:BANKNIFTY21JUNFUT";
const contractName = "BANKNIFTY21JUN"
var ceSellOrderID = "", peSellOrderID = "", ceSellSLOrderID = "", peSellSLOrderID = "";
var ceSellAvgPrice = "", peSellAvgPrice = "", ceSLPrice = "", peSLPrice = "";
var isMarginReqMet;
var marginAmount;
var api_key = "",
	secret = "",
	request_token = "",
	access_token = "",
	bnfSpot = "";
var options = {
	"api_key": api_key,
	"debug": false
};

function contractNameBuilder(strike, options) {
	//console.log(contractName.concat(strike).concat(options))
	return contractName.concat(strike).concat(options);
}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
 }

async function callPupetter() {
	console.log("inside Pupetter")
	const browser = await puppeteer.launch({ headless: true });
	const page = await browser.newPage();
	console.log('there');
	await page.goto("https://kite.trade/connect/login?api_key=xameg1eplldsbcpf");
	await page.waitForSelector("#userid");
	await page.waitForSelector("#password");
	await page.type("#userid", "", { delay: 100 });
	await page.type("#password", "", { delay: 100 });
	await page.click("button.button-orange")

	await page.waitForSelector("#pin");
	await page.type("#pin", "", { delay: 100 });
	await page.click("button.button-orange")

	await page.waitForTimeout(1000);
	//console.log(page);
	const pages = await browser.pages()
	const finalURL = page.url();
	//const myParam = urlParams.get('request_token');
	console.log(" REQUEST TOKEN BEFORE GETTING FROM FINAL", request_token)
	//request_token = finalURL.slice(38, 70);
	console.log("FINAL REDIRECTED URL", finalURL);
	const params = new URLSearchParams(finalURL)
	console.log(" PARAMS::", params)
	const request_token_retrieved = params.get('https://www.google.com/?request_token') != null ?
		params.get('https://www.google.com/?request_token') : params.get('request_token');
	request_token = request_token_retrieved;
	//console.log(" request_token", code);
	console.log(" REQUEST TOKEN AFTER GETTING FROM FINAL", request_token)
	//   await page.screenshot({path: 'example.png'});	
	await browser.close();
	console.log("Closing pupetter")
};

callPupetter().then(() => {
	kc = new KiteConnect(options);
	kc.setSessionExpiryHook(sessionHook);

	if (!access_token) {
		console.log(" NO ACCESS TOKEN")
		kc.generateSession(request_token, secret)
			.then(function (response) {
				console.log("Response", response);
				init();
			})
			.catch(function (err) {
				console.log(err);
			})
	} else {
		console.log("RECEIVED ACCESS TOKEN")
		kc.setAccessToken(access_token);
		init();
	}
});


async function init() {
	console.log(" INIT METHOD")
	console.log(kc.getLoginURL())
	//getQuote(["NSE:RELIANCE"]);
	console.log(" CHECK MARGIN BEFORE PROCEEDING")
	await getMargins();

	(marginAmount < 18000) ? isMarginReqMet = false : isMarginReqMet = true;
	console.log(" isMarginReqMet ::", isMarginReqMet)


	console.log("GETTING BNF FUT PRICE")
	await getBNFSPOTLTP();

	console.log("BNF SPOT LTP", bnfSpot)

	var bnfSpotRoundedUP = Math.ceil(bnfSpot / 100) * 100;

	console.log("ROUNDED UP", bnfSpotRoundedUP)

	//sleep(3000);

	var firstLeg = Math.round(bnfSpotRoundedUP) - 200;
	var firstLegContract = String(contractNameBuilder(firstLeg, "CE"));
	console.log(" FIRST LEG CONSTURCTED ::", firstLegContract)
	var secondLeg = Math.round(bnfSpotRoundedUP) + 300;
	var secondLegContract = String(contractNameBuilder(secondLeg, "PE"));
	console.log(" SECOND LEG CONSTURCTED ::", secondLegContract)
	console.log("CE leg", firstLeg);
	console.log("PE leg", secondLeg);

	//regularOrderPlaceTEST("regular");

	if (isMarginReqMet) {
		//BNF OPTIONS STANGGLE SELL ORDERS
		await sellCEContract(firstLegContract);
		await sellPEcontract(secondLegContract);
		//peSellOrderID = await regularOptionsSellTest(secondLegContract);

		console.log(" CE SELL ORDER ID", ceSellOrderID);
		console.log(" PE SELL ORDER ID", peSellOrderID);

		//await getOrderDetailsForCESell(String(ceSellOrderID));
		//await getOrderDetailsForPESell(String(peSellOrderID));

		console.log("ceSellAvgPrice ::", ceSellAvgPrice)
		console.log("peSellAvgPrice ::", peSellAvgPrice)
		//getOrderTrades(peSellOrderID);

		//PLACE THE SL BUY ORDER
		await placeStopLossForCESell(String(firstLegContract), Math.ceil(ceSellAvgPrice * 1.3));
		await placeStopLossForPESell(String(secondLegContract), Math.ceil(peSellAvgPrice * 1.3));

		console.log(" CE SL SELL ORDER ID", ceSellSLOrderID);
		console.log(" PE SL SELL ORDER ID", peSellSLOrderID);

		//WAIT FOR THE TWO SL ORDERS TO
		while (true) {
			var currentTime = new Date();
			var currentOffset = currentTime.getTimezoneOffset();
			var ISTOffset = 330;   // IST offset UTC +5:30 
			ISTTime = new Date(currentTime.getTime() + (ISTOffset + currentOffset) * 60000);
			hoursIST = ISTTime.getHours()
			minutesIST = ISTTime.getMinutes()
			console.log(" HOURS:", hoursIST, "MINUTES::", minutesIST);
			if (hoursIST === 00 && minutesIST === 20) {
				console.log(" 5:04 condition met exiting")
				//CANCEL THE PENDING BUY StopLoss ORDERS
				await cancelOrders("regular", String(ceSellSLOrderID));
				await cancelOrders("regular", String(peSellSLOrderID));
				//EXIT ANY POSITIONS AT MARKET PRICE
				await exitCurrentOrderInMarketPrice("regular", String(ceSellOrderID));
				await exitCurrentOrderInMarketPrice("regular", String(peSellOrderID));
				break;
			}
			await sleep(60000);
		}
	}


	//Get the margins 
	//getMargins("equity");
	//console.log(" LAST PRICE ::", getLTP(["NFO:BANKNIFTY21JUNFUT"]))

	//REMOVE TOKEN ON EXIT
	console.log(" REMOVING ACCESS TOKEN")
	invalidateAccessToken();
}


async function generateRequestTokenFromZerodha() {

	return new Promise(async (resolve, reject) => {

		console.log(" BEFORE REQUESTING REQUEST TOKEN")
		callkiteAPI();
		console.log(finalURL);
		console.log(request_token);
		console.log('yes');
		//   await page.screenshot({path: 'example.png'});	
		await browser.close();

		if (!request_token) {
			resolve();
		} else {
			reject(" Failed to get request token")
		}

	});

}


async function callkiteAPI() {


}

function sessionHook() {
	console.log("User loggedout");
}

function getProfile() {
	kc.getProfile()
		.then(function (response) {
			console.log(response)
		}).catch(function (err) {
			console.log(err);
		});
}

async function getMargins(segment) {
	await kc.getMargins(segment)
		.then(function (response) {
			//console.log(" MARGIN OBJ ::", response)
			console.log("MARGIN details", response.equity.available.cash);
			marginAmount = response.equity.available.cash;
		}).catch(function (err) {
			console.log(err);
		});
}

function getPositions() {
	kc.getPositions()
		.then(function (response) {
			console.log(response);
		}).catch(function (err) {
			console.log(err);
		});
}

function getHoldings() {
	kc.getHoldings()
		.then(function (response) {
			console.log(response);
		}).catch(function (err) {
			console.log(err.response);
		});
}

function getOrders() {
	kc.getOrders()
		.then(function (response) {
			console.log(response);
		}).catch(function (err) {
			console.log(err);
		});
}

function getOrderHistory() {
	kc.getOrders()
		.then(function (response) {
			if (response.length === 0) {
				console.log("No orders.")
				return
			}

			kc.getOrderHistory(response[0].order_id)
				.then(function (response) {
					console.log(response);
				}).catch(function (err) {
					console.log(err);
				});
		}).catch(function (err) {
			console.log(err);
		});
}

function getTrades() {
	kc.getTrades()
		.then(function (response) {
			console.log(response);
		}).catch(function (err) {
			console.log(err);
		});
}

function getOrderTrades() {
	kc.getOrders()
		.then(function (response) {
			var completedOrdersID;
			for (var order of response) {
				if (order.status === kc.STATUS_COMPLETE) {
					completedOrdersID = order.order_id;
					break;
				}
			}

			if (!completedOrdersID) {
				console.log("No completed orders.")
				return
			}

			kc.getOrderTrades(completedOrdersID)
				.then(function (response) {
					console.log(response);
				}).catch(function (err) {
					console.log(err);
				});
		}).catch(function (err) {
			console.log(err);
		});
}


async function getOrderDetailsForCESell(order_id) {
	await kc.getOrderTrades(order_id)
		.then(function (response) {
			console.log("ORDER DETAILS", response);
			//assigningVariable = response[0].average_price;
			//console.log(" ASSISGNED VALUE::", assigningVariable)
			ceSellAvgPrice = response[0].average_price;
		}).catch(function (err) {
			console.log(err);
		});
}

async function getOrderDetailsForPESell(order_id) {
	await kc.getOrderTrades(order_id)
		.then(function (response) {
			console.log("ORDER DETAILS", response);
			//assigningVariable = response[0].average_price;
			//console.log(" ASSISGNED VALUE::", assigningVariable)
			peSellAvgPrice = response[0].average_price;
		}).catch(function (err) {
			console.log(err);
		});
}


function getInstruments(exchange) {
	kc.getInstruments(exchange).then(function (response) {
		console.log(response);
	}).catch(function (err) {
		console.log(err);
	})
}

function getQuote(instruments) {
	kc.getQuote(instruments).then(function (response) {
		console.log(response);
	}).catch(function (err) {
		console.log(err);
	})
}

function getOHLC(instruments) {
	kc.getOHLC(instruments).then(function (response) {
		console.log(response);
	}).catch(function (err) {
		console.log(err);
	})
}

async function getBNFSPOTLTP() {
	await kc.getLTP(bnfFuture).then(function (response) {
		console.log("BNF PRICE", response);
		console.log(" JSON", JSON.stringify(response))
		bnfSpot = response[bnfFuture].last_price;
		console.log(" BNF SPOT", bnfSpot)
	}).catch(function (err) {
		console.log(err);
	})
}


function getLTP(instruments) {
	kc.getLTP(instruments).then(function (response) {
		console.log("BNF PRICE", response);
		console.log(" JSON", JSON.stringify(response))
		bnfSpot = response[bnfFuture].last_price;
		console.log(" BNF SPOT", bnfSpot)
	}).catch(function (err) {
		console.log(err);
	})
}

function getHistoricalData(instrument_token, interval, from_date, to_date, continuous) {
	kc.getHistoricalData(instrument_token, interval, from_date, to_date, continuous)
		.then(function (response) {
			console.log(response);
		}).catch(function (err) {
			console.log(err);
		});
}

function getMFInstruments() {
	kc.getMFInstruments()
		.then(function (response) {
			console.log(response);
		}).catch(function (err) {
			console.log(err);
		});
}

function getMFOrders() {
	kc.getMFOrders()
		.then(function (response) {
			console.log(response);
		}).catch(function (err) {
			console.log(err);
		});
}

function getMFSIPS() {
	kc.getMFSIPS()
		.then(function (response) {
			console.log(response);
		}).catch(function (err) {
			console.log(err);
		});
}

function invalidateAccessToken(access_token) {
	kc.invalidateAccessToken(access_token)
		.then(function (response) {
			console.log(response);
			testOrders();
		}).catch(function (err) {
			console.log(err.response);
		});
}

function regularOrderPlace(variety) {
	kc.placeOrder(variety, {
		"exchange": "NSE",
		"tradingsymbol": "RELIANCE",
		"transaction_type": "BUY",
		"quantity": 1,
		"product": "MIS",
		"order_type": "MARKET"
	}).then(function (resp) {
		console.log(resp);
	}).catch(function (err) {
		console.log(err);
	});
}


function regularOrderPlaceTEST(variety) {
	kc.placeOrder(variety, {
		"exchange": "NFO",
		"tradingsymbol": "BANKNIFTY21JUN34000CE",
		"transaction_type": "BUY",
		"quantity": 1,
		"product": "MIS",
		"order_type": "MARKET"
	}).then(function (resp) {
		console.log(resp);
	}).catch(function (err) {
		console.log(err);
	});
}



async function sellCEContract(contract) {
	await kc.placeOrder("regular", {
		"exchange": "NFO",
		"tradingsymbol": contract,
		"transaction_type": "SELL",
		"quantity": 25,
		"product": "MIS",
		"order_type": "MARKET",
	}).then(function (resp) {
		console.log(resp);
		console.log(" ORDER ID ::", resp.order_id)
		ceSellOrderID = resp.order_id;
		ceSellAvgPrice = resp.average_price;
	}).catch(function (err) {
		console.log(err);
	});
}

async function sellPEcontract(contract) {
	await kc.placeOrder("regular", {
		"exchange": "NFO",
		"tradingsymbol": contract,
		"transaction_type": "SELL",
		"quantity": 25,
		"product": "MIS",
		"order_type": "MARKET",
	}).then(function (resp) {
		console.log(resp);
		console.log(" ORDER ID ::", resp.order_id)
		peSellOrderID = resp.order_id;
		peSellAvgPrice = resp.average_price;
	}).catch(function (err) {
		console.log(err);
	});
}


async function regularOptionsSell(contract) {
	await kc.placeOrder("regular", {
		"exchange": "NFO",
		"tradingsymbol": contract,
		"transaction_type": "SELL",
		"quantity": 25,
		"product": "MIS",
		"order_type": "MARKET",
	}).then(function (resp) {
		console.log(resp);
		return resp.order_id;
	}).catch(function (err) {
		console.log(err);
	});
}


async function placeStopLossForCESell(contract, trigger_price) {
	await kc.placeOrder("regular", {
		"exchange": "NFO",
		"tradingsymbol": contract,
		"transaction_type": "BUY",
		"quantity": 25,
		"product": "MIS",
		"order_type": "SL-M",
		"trigger_price": trigger_price
	}).then(function (resp) {
		console.log(resp);
		ceSellSLOrderID = resp.order_id;
	}).catch(function (err) {
		console.log(err);
	});
}

async function placeStopLossForPESell(contract, trigger_price) {
	await kc.placeOrder("regular", {
		"exchange": "NFO",
		"tradingsymbol": contract,
		"transaction_type": "BUY",
		"quantity": 25,
		"product": "MIS",
		"order_type": "SL-M",
		"trigger_price": trigger_price
	}).then(function (resp) {
		console.log(resp);
		peSellSLOrderID = resp.order_id;
	}).catch(function (err) {
		console.log(err);
	});
}


function bracketOrderPlace() {
	kc.placeOrder(kc.VARIETY_BO, {
		"exchange": "NSE",
		"tradingsymbol": "RELIANCE",
		"transaction_type": "BUY",
		"order_type": "LIMIT",
		"quantity": 1,
		"price": 900,
		"squareoff": 10,
		"stoploss": 10,
		"validity": "DAY"
	},
	).then(function (resp) {
		console.log(resp);
	}).catch(function (err) {
		console.log(err);
	});
}


async function exitCurrentOrderInMarketPrice() {

	await function exitCurrentOrders(variety, order_id) {
		kc.exitOrder(variety, order_id, {}).then(function (resp) {
			console.log(resp);
		}).catch(function (err) {
			console.log(err);
		});
	}
}

function modifyOrder(variety) {
	var tradingsymbol = "RELIANCE";
	var exchange = "NSE";
	var instrument = exchange + ":" + tradingsymbol;

	function modify(variety, order_id) {
		kc.modifyOrder(variety, order_id, {
			quantity: 2
		}).then(function (resp) {
			console.log(resp);
		}).catch(function (err) {
			console.log(err);
		});
	}

	kc.getLTP([instrument])
		.then(function (resp) {
			kc.placeOrder(variety, {
				"exchange": exchange,
				"tradingsymbol": tradingsymbol,
				"transaction_type": "BUY",
				"quantity": 1,
				"product": "MIS",
				"order_type": "LIMIT",
				"price": resp[instrument].last_price - 5
			}).then(function (resp) {
				modify(variety, resp.order_id);
			}).catch(function (err) {
				console.log("Order place error", err);
			});
		}).catch(function (err) {
			console.log(err);
		});
}

async function cancelOrders(variety, order_id) {
	await function cancel(variety, order_id) {
		kc.cancelOrder(variety, order_id)
			.then(function (resp) {
				console.log(resp);
			}).catch(function (err) {
				console.log(err);
			});
	}
}


function cancelOrder(variety) {
	var tradingsymbol = "RELIANCE";
	var exchange = "NSE";
	var instrument = exchange + ":" + tradingsymbol;

	function cancel(variety, order_id) {
		kc.cancelOrder(variety, order_id)
			.then(function (resp) {
				console.log(resp);
			}).catch(function (err) {
				console.log(err);
			});
	}

	kc.getLTP([instrument])
		.then(function (resp) {
			kc.placeOrder(variety, {
				"exchange": exchange,
				"tradingsymbol": tradingsymbol,
				"transaction_type": "BUY",
				"quantity": 1,
				"product": "MIS",
				"order_type": "LIMIT",
				"price": resp[instrument].last_price - 5
			}).then(function (resp) {
				cancel(variety, resp.order_id);
			}).catch(function (err) {
				console.log("Order place error", err);
			});
		}).catch(function (err) {
			console.log(err);
		});
}

function getGTT(trigger_id) {
	if (trigger_id) {
		kc.getGTTs().then(function (resp) {
			console.log(resp)
		}).catch(function (error) {
			console.log(error)
		})
	} else {
		kc.getGTT(trigger_id).then(function (resp) {
			console.log(resp)
		}).catch(function (error) {
			console.log(error)
		})
	}
}

function placeGTT() {
	kc.placeGTT({
		trigger_type: kc.GTT_TYPE_OCO,
		tradingsymbol: "SBIN",
		exchange: "NSE",
		trigger_values: [300, 400],
		last_price: 318,
		orders: [{
			transaction_type: kc.TRANSACTION_TYPE_SELL,
			quantity: 1,
			product: kc.PRODUCT_CNC,
			order_type: kc.ORDER_TYPE_LIMIT,
			price: 300
		}, {
			transaction_type: kc.TRANSACTION_TYPE_SELL,
			quantity: 1,
			product: kc.PRODUCT_CNC,
			order_type: kc.ORDER_TYPE_LIMIT,
			price: 400
		}]
	}).then(function (resp) {
		console.log(resp)
	}).catch(function (error) {
		console.log(error)
	})
}

function modifyGTT(trigger_id) {
	kc.modifyGTT(trigger_id, {
		trigger_type: kc.GTT_TYPE_OCO,
		tradingsymbol: "SBIN",
		exchange: "NSE",
		trigger_values: [301, 401],
		last_price: 318,
		orders: [{
			transaction_type: kc.TRANSACTION_TYPE_SELL,
			quantity: 1,
			product: kc.PRODUCT_CNC,
			order_type: kc.ORDER_TYPE_LIMIT,
			price: 300
		}, {
			transaction_type: kc.TRANSACTION_TYPE_SELL,
			quantity: 1,
			product: kc.PRODUCT_CNC,
			order_type: kc.ORDER_TYPE_LIMIT,
			price: 400
		}]
	}).then(function (resp) {
		console.log(resp)
	}).catch(function (error) {
		console.log(error)
	})
}

function deleteGTT(trigger_id) {
	kc.deleteGTT(trigger_id).then(function (resp) {
		console.log(resp)
	}).catch(function (error) {
		console.log(error)
	})
}
